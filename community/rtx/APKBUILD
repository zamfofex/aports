# Maintainer: Jeff Dickey <alpine@rtx.pub>
pkgname=rtx
pkgver=2023.11.4
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://rtx.pub"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdxcode/rtx/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	git init .
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin rtx
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/rtx \
		-t "$pkgdir"/usr/bin/
}

sha512sums="
b5fbfe691459333b14330c52828a08aebf7ce88c14c7710f0ed8906742400bb4dc6eb57ece9815b61ecfe3c84a1712634fbc9188e07ed993c22b48b9d0aba094  rtx-2023.11.4.tar.gz
"
