# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=jql
pkgver=7.0.6
pkgrel=0
pkgdesc="A JSON Query Language CLI tool"
url="https://github.com/yamafaktory/jql"
arch="all"
license="MIT"
makedepends="cargo cargo-auditable"
source="https://github.com/yamafaktory/jql/archive/jql-v$pkgver.tar.gz"
options="net" # fetch dependencies
builddir="$srcdir/jql-jql-v$pkgver"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	# core::tests::get_property_as_index seems to be broken
	cargo test --frozen -- \
		--skip core::tests::get_property_as_index
}

package() {
	install -D -m755 target/release/jql -t "$pkgdir"/usr/bin/
}

sha512sums="
1e4c101ec1ac57183d62a154c8e4ade852c53b671c028c7b07057604a1753e83224f7dc88359d2f071fcce6ef43e803863a73b78159fc389f7712825eb68b9c7  jql-v7.0.6.tar.gz
"
