# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=xkcdpass
pkgver=1.19.5
pkgrel=0
pkgdesc="Generate secure multiword passwords/passphrases"
url="https://github.com/redacted/XKCD-password-generator"
arch="noarch"
license="BSD-3-Clause"
depends="py3-xkcdpass"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
subpackages="py3-$pkgname-pyc py3-$pkgname:py3"
source="https://files.pythonhosted.org/packages/source/x/xkcdpass/xkcdpass-$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 tests/test_xkcdpass.py
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
	rm -r "$pkgdir"/usr/lib/python*/site-packages/examples
}

py3() {
	pkgdesc="Python library for $pkgname"

	amove usr/lib/python*
}

sha512sums="
8a560247cb860377ef723b26610a6f784a891b4c8dfb2c5b55187df70e326d6ff2f1d31128dfc4e7136cbc15108c470658d896056177900e0eb419d14458348f  xkcdpass-1.19.5.tar.gz
"
