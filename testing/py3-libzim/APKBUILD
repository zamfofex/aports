
# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=py3-libzim
pkgver=3.2.0
pkgrel=0
pkgdesc="Python binding for libzim"
url="https://github.com/openzim/python-libzim"
arch="all"
license="GPL-3.0-or-later"
depends="libzim"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	cython
	python3-dev
	libzim-dev
	"
checkdepends="py3-pytest py3-pytest-cov"
source="py3-libzim-$pkgver.tar.gz::https://github.com/openzim/python-libzim/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/python-libzim-$pkgver"

build() {
	export DONT_DOWNLOAD_LIBZIM=yes
	export USE_SYSTEM_LIBZIM=yes

	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
41697b862fabe5e57680044462a798d6205862f84c4731480bffeede579eaa4da8cbe832370be71b651c2a60f6fd58819d67ef1c732b92c8254809bca16a185e  py3-libzim-3.2.0.tar.gz
"
