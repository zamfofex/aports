# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=opentofu
pkgver=1.6.0_alpha4
pkgrel=0
pkgdesc="OpenTofu lets you declaratively manage your cloud infrastructure"
url="https://opentofu.org"
# x86, armhf, armv7: several tests fail
arch="all !x86 !armhf !armv7"
license="MPL-2.0"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/opentofu/opentofu/archive/refs/tags/v${pkgver/_/-}.tar.gz"
builddir="$srcdir/$pkgname-${pkgver/_/-}"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local ldflags="-X 'github.com/opentofu/opentofu/version.dev=no'"
	go build -ldflags "$ldflags" ./cmd/tofu
}

check() {
	go test -v ./...
}

package() {
	install -Dm0755 tofu -t "$pkgdir"/usr/bin/
}

sha512sums="
3cc716e26021abdcfd2010682c3acfff9ce698f5b73f39d6f77cf9cc76e8b50d7e09e9efc398716278ac3e805371ff91795695bfc8b2ef2ff3fa72f705738f30  opentofu-1.6.0_alpha4.tar.gz
"
