# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=cargo-update
pkgver=13.2.0
pkgrel=0
pkgdesc="cargo subcommand for checking and applying updates to installed executables"
url="https://github.com/nabijaczleweli/cargo-update"
arch="all"
license="MIT"
makedepends="cargo cargo-auditable ronn curl-dev libgit2-dev libssh-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/nabijaczleweli/cargo-update/archive/refs/tags/v$pkgver.tar.gz"

export LIBSSH2_SYS_USE_PKG_CONFIG=1

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		git2 = { rustc-link-lib = ["git2"] }
	EOF

	cargo fetch --target="$CTARGET"
}

build() {
	cargo auditable build --frozen --release
	ronn --roff --organization="CARGO-UPDATE $pkgver" man/*.md
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 -t "$pkgdir"/usr/bin/ \
		target/release/cargo-install-update \
		target/release/cargo-install-update-config

	install -Dm644 man/cargo-install-update*.1 \
		-t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
6ffc187b893363a52e72a22da2ad0d188fc8a670185377448e70d3e81bf73729eb940dbe0c8b3920fd820bfbbc927645d0f8b0b0631ce37a4d8257fa8ace4809  cargo-update-13.2.0.tar.gz
"
